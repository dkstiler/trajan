from django.db import models
from django.contrib.auth import get_user_model

from home.models import Persona
User = get_user_model()

#class Room(models.Model):
#    """
#    A room for people to chat in.
#    """
#    owner = models.ForeignKey(Persona,
#                              related_name="room",
#                              on_delete=models.CASCADE)
#
#    # Room title
#    title = models.CharField(max_length=255)
#
#    def __str__(self):
#        return self.title
#
#    @property
#    def group_name(self):
#        """
#        Returns the Channels Group name that sockets should subscribe to to get sent
#        messages as they are generated.
#        """
#        return "room-%s" % self.id

class ClientChannel(models.Model):

    client = models.ForeignKey(User,
                              related_name='clientchannel',
                              on_delete=models.CASCADE)
    
    channel_name = models.CharField(max_length=50)

    def __repr__(self):
        return '{} : {}'.format(self.client, self.channel_name)
