from django.urls import path
from . import views

app_name = 'ajax'

urlpatterns = [
    path('pqc_demo_gen/', views.pqc_demo_gen),
    path('pqc_demo_enc/', views.pqc_demo_enc),
    path('pqc_demo_dec/', views.pqc_demo_dec),
]
