from django.shortcuts import render

from django.http import HttpResponse
from django.views.decorators.http import require_POST
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django_ajax.decorators import ajax

# Create your views here.

from home.forms import PQCdemoForm

import valens

@ajax
@require_POST
def main(request):
    """
    Main AJAX view
    """
    
    msg = 'data?'

    if request.POST['mydata']:
        msg = request.POST['mydata']
    
    return {'msg': msg} 

@ajax
@require_POST
def pqc_demo_gen(request):

    demo = PQCdemoForm(data=request.POST)
    if not demo.is_valid():
        return {'msg': 'invalid'}

    V = valens.Valens()

    return {'pubkey': V.getPubKey(),
            'privkey': V.getPrivKey()}
    
@ajax
@require_POST
def pqc_demo_enc(request):
    
    demo = PQCdemoForm(data=request.POST)
    if not demo.is_valid():
        return {'msg': 'invalid'}

    try:
        V = valens.Valens()
        V.importPubKey(demo.cleaned_data['pubkey'])
        out = V.encryptMsg(demo.cleaned_data['plaintext'])
    except Exception as e:
        return {'msg': 'something went wrong'}

    return {'ciphertext': out}
 
@ajax
@require_POST
def pqc_demo_dec(request):
   
    demo = PQCdemoForm(data=request.POST)
    if not demo.is_valid():
        return {'msg': 'invalid'}

    try:
        V = valens.Valens()
        V.importPubKey(demo.cleaned_data['pubkey'])
        V.importPrivKey(demo.cleaned_data['privkey'])
        out = V.decryptMsg(demo.cleaned_data['ciphertext'])
    except Exception as e:
        return {'msg': 'something went wrong'}

    return {'plaintext': out}


