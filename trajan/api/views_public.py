
from rest_framework import generics
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.views import APIView

from api.serializers_public import PublicEncKeyPairSerializer, PublicPersonaSerializer, PublicUserSerializer

from home.models import Persona, EncKeyPair
from django.contrib.auth import get_user_model
User = get_user_model()

class PublicEncKeyPairList(generics.ListAPIView):
    """
    Publicly available list view of public encryption keys
    """
    queryset = EncKeyPair.objects.all()
    serializer_class = PublicEncKeyPairSerializer
    permission_classes = (AllowAny,)

class PublicEncKeyPairDetail(generics.RetrieveAPIView):
    """
    Publicly available detail view of public encryption keys
    """
    queryset = EncKeyPair.objects.all()
    serializer_class = PublicEncKeyPairSerializer
    permission_classes = (AllowAny,)

class PublicPersonaDetail(generics.RetrieveAPIView):
    """
    Publicly available detail view of personas
    """
    queryset = Persona.objects.all()
    serializer_class = PublicPersonaSerializer
    permission_classes = (AllowAny,)

class PublicPersonaNameDetail(generics.RetrieveAPIView):
    """
    Publicly available detail view of personas
    """
    queryset = Persona.objects.all()
    serializer_class = PublicPersonaSerializer
    permission_classes = (AllowAny,)
    lookup_field = 'name'

class PublicEmailDetail(generics.RetrieveAPIView):
    """
    Publicly available detail view for encryption keys from email
    """
    queryset = User.objects.all()
    serializer_class = PublicUserSerializer
    lookup_field = 'email'
