from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from . import views
from . import views_public

app_name = 'api'

urlpatterns = [
    path('users/', views.UserList.as_view()),
    path('users/<int:pk>/', views.UserDetail.as_view()),
    path('personas/', views.PersonaList.as_view()),
    path('personas/<int:pk>/', views.PersonaDetail.as_view()),
    path('enckeypair/', views.EncKeyPairList.as_view()),
    path('enckeypair/<int:pk>/', views.EncKeyPairDetail.as_view()),
    path('signkeypair/', views.SignKeyPairList.as_view()),
    path('signkeypair/<int:pk>/', views.SignKeyPairDetail.as_view()),
    #path('rooms/', views.RoomList.as_view()),
    #path('rooms/<int:pk>/', views.RoomDetail.as_view()),
    path('roomkey/', views.RoomKeyList.as_view()),
    path('roomkey/<int:pk>/', views.RoomKeyDetail.as_view()),

    path('public/ekp/', views_public.PublicEncKeyPairList.as_view()),
    path('public/ekp/<int:pk>/', views_public.PublicEncKeyPairDetail.as_view()),

    path('public/personas/<int:pk>/', views_public.PublicPersonaDetail.as_view()),
    path('public/personas/<str:name>/', views_public.PublicPersonaNameDetail.as_view()),
    path('public/email/<str:email>/', views_public.PublicEmailDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
