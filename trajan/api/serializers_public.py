
from rest_framework import serializers

from . import serializers as api_serializers

from home.models import Persona, EncKeyPair
from django.contrib.auth import get_user_model
User = get_user_model()

class PublicEncKeyPairSerializer(serializers.ModelSerializer):
    class Meta:
        model = EncKeyPair
        fields = ('id','name','pubkey')

class PublicPersonaSerializer(serializers.ModelSerializer):
    #enckeypair = PublicEncKeyPairSerializer(many=True, read_only=True)
    #enckeypair = serializers.SerializerMethodField('get_enckeypair')
    enckeypair = serializers.SerializerMethodField()
    class Meta:
        model = Persona
        #fields = ('id', 'name', 'enckeypair')
        fields = ('enckeypair',)

    def get_enckeypair(self, persona):
        enckeypair = EncKeyPair.objects.filter(persona=persona).first() 
        return PublicEncKeyPairSerializer(enckeypair, many=False).data
        

class PublicUserSerializer(serializers.ModelSerializer):
    #persona = PublicPersonaSerializer(many=True, read_only=True)
    persona = serializers.SerializerMethodField()
    class Meta:
        model = User
        fields = ('persona',)

    def get_persona(self, user):
        persona = Persona.objects.filter(owner=user).first()
        return PublicPersonaSerializer(persona, many=False).data
