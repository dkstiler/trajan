from django.contrib.auth.models import Group
from django.contrib.auth import get_user_model
User = get_user_model()

from rest_framework import serializers

from home.models import Persona, EncKeyPair, SignKeyPair
#from chat.models import Room
from .models import RoomKey


class RoomKeySerializer(serializers.ModelSerializer):
    #room = serializers.CharField(source='room.title')
    #ekp = serializers.CharField(source='ekp.id', read_only=True)
    class Meta:
        model = RoomKey
        fields = ('id','ekp','enckey','encgroup','encgroup_enctype','encname','encname_enctype')

class EncKeyPairSerializer(serializers.ModelSerializer):
    #persona = serializers.CharField(source='persona.name')
    roomkey = RoomKeySerializer(many=True, read_only=True)
    class Meta:
        model = EncKeyPair
        fields = ('id', 'name', 'pubkey', 'encprivkey', 'privkey_enctype', 'roomkey')

class SignKeyPairSerializer(serializers.ModelSerializer):
    persona = serializers.CharField(source='persona.name')
    class Meta:
        model = SignKeyPair
        fields = ('id', 'persona', 'name', 'pubkey', 'encprivkey', 'privkey_enctype')

class PersonaSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')
    #enckeypair = serializers.PrimaryKeyRelatedField(many=True, queryset=EncKeyPair.objects.all())
    enckeypair = EncKeyPairSerializer(many=True, read_only=True)
    #signkeypair = serializers.PrimaryKeyRelatedField(many=True, queryset=SignKeyPair.objects.all())
    signkeypair = SignKeyPairSerializer(many=True, read_only=True) 
    class Meta:
        model = Persona
        fields = ('id', 'owner', 'name', 'enckeypair', 'signkeypair')

#class RoomSerializer(serializers.ModelSerializer):
#    #owner = PersonaSerializer(read_only=True)
#    class Meta:
#        model = Room
#        fields = ('id', 'owner', 'title')

class UserSerializer(serializers.ModelSerializer):
    #persona = serializers.PrimaryKeyRelatedField(many=True, queryset=Persona.objects.all())
    persona = serializers.SlugRelatedField(many=True, slug_field='name', queryset=Persona.objects.all())
    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'persona')
        #fields = '__all__'

#class UserSerializer(serializers.HyperlinkedModelSerializer):
#    class Meta:
#        model = User
#        fields = ('url', 'email', 'groups')

class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')
