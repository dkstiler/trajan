from django.http import HttpResponse, HttpResponseForbidden, JsonResponse
from django.shortcuts import render, redirect
from django.contrib.auth import login, logout, authenticate
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from django.urls import reverse_lazy
from django.views import generic
from django.core.mail import EmailMessage
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
#from django.template.context_processors import csrf
#from django.middleware.csrf import get_token

from .tokens import account_activation_token
from .forms import CustomUserCreationForm, PQCdemoForm 

import json

# Create your views here.

def apilogin(request):
    """
    Login from python client
    """
    if request.POST:
        if 'username' in request.POST and 'password' in request.POST:

            username = request.POST['username']
            password = request.POST['password']
            login_user = authenticate(username=username,
                                      password=password,
                                      request=request)

            if login_user and login_user.is_active:
                login(request, login_user)
                return HttpResponse('Logged in!')

    return HttpResponseForbidden('Unsuccessful') 


@require_POST
def apilogout(request):
    """
    Logout from python client
    """
    logout(request)
    return HttpResponse('Logged out')
    
def index(request):
    """
    Main view
    """
    
    return render(request, "index.html", {
        "content": [],
        "title": "My Title is AWESOME!",
        "mystuff": "When in the course of human events",
    })

def about(request):
    """
    About me
    """

    return render(request, "about.html", {
        "content": [],
        "title": "About Steven Foerster",
    })

class pqc_demo(generic.CreateView):
    """
    Post Quantum Cryptography demo
    """
    form_class = PQCdemoForm
    success_url = '#'
    template_name = 'pqc_demo.html'

class register(generic.CreateView):
    """
    Registration form
    """
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('index')
    template_name = 'register.html'

def registration(request):
    """
    Process the registration.
    """
    
    # check if POST
    if request.method != 'POST':
        return redirect('/register/') 
        
    # load and validate form
    form = CustomUserCreationForm(request.POST)
    if not form.is_valid():
        return redirect('/register/')

    return HttpResponse(form.cleaned_data)
    
    # create user object
    user = form.save(commit=False)
    user.is_active = False
    user.save()

    # create confirmation email
    current_site = get_current_site(request)
    mail_subject = "Activate your account"
    message = render_to_string('activate_email.html', {
        'user': user,
        'domain': current_site.domain,
        'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode(),
        'token': account_activation_token.make_token(user),
    })
    to_email = form.cleaned_data.get('email')
    email = EmailMessage(
        mail_subject,
        message,
        to=[to_email],
    )
    email.send()

    return HttpResponse('Please confirm your email address to complete registration.')

def activate(request, uidb64, token):
    """
    Activate account.
    """
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user)
        return HttpResponse('Your account has been confirmed!')
    
    return HttpResponse('Activation link is invalid.')
