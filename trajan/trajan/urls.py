from django.urls import path, re_path, include
#from django.conf.urls import url, include

from django.conf.urls.static import static
from django.conf import settings

from django.contrib import admin
#from django.contrib.auth.views import login, logout
from django.contrib.auth import views as auth_views 
#from chat.views import index
from home import views as home_views
#from ajax import views as ajax_views

# BEGIN AXES/ALLAUTH INTEGRATION
from allauth.account.views import LoginView
from axes.decorators import axes_dispatch, axes_form_invalid
from django.utils.decorators import method_decorator

from home.forms import AllauthCompatLoginForm

LoginView.dispatch = method_decorator(axes_dispatch)(LoginView.dispatch)
LoginView.form_invalid = method_decorator(axes_form_invalid)(LoginView.form_invalid)
# END AXES/ALLAUTH INTEGRATION

#from rest_framework import routers
#from api import views as api_views

#router = routers.DefaultRouter()
#router.register(r'users', api_views.UserViewSet)
#router.register(r'groups', api_views.GroupViewSet)

from django.contrib.auth.decorators import login_required
## require user to be logged in to access the admin site
#admin.site.login = login_required(admin.site.login)

from .startup import run_once


from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter
from rest_auth.registration.views import SocialLoginView, SocialConnectView, SocialAccountListView, SocialAccountDisconnectView
class FacebookLogin(SocialLoginView):
    adapter_class = FacebookOAuth2Adapter

class FacebookConnect(SocialConnectView):
    adapter_class = FacebookOAuth2Adapter

urlpatterns = [
    path('', home_views.index),
    path('about/', home_views.about),
    #path('register/', home_views.register.as_view()),
    #path('registration/', home_views.registration),
    #re_path(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', home_views.activate),
    path('pqc/', home_views.pqc_demo.as_view()),
    #path('accounts/login/', login),
    #path('accounts/logout/', logout),
    #path('accounts/login/', auth_views.LoginView.as_view()),
    #path('accounts/logout/', auth_views.LogoutView.as_view()),
    path('apilogin/', home_views.apilogin),
    path('apilogout/', home_views.apilogout),
    path('accounts/login/', LoginView.as_view(form_class=AllauthCompatLoginForm), name="account_login"), # AXES/ALLAUTH INTEGRATION
    path('accounts/', include('allauth.urls')),
    path('rest-auth/', include('rest_auth.urls')),
    path('rest-auth/registration/', include('rest_auth.registration.urls')),
    path('rest-auth/facebook/', FacebookLogin.as_view(), name='fb_login'),
    path('rest-auth/facebook/connect/', FacebookConnect.as_view(), name='fb_connect'),
    path('socialaccounts/', SocialAccountListView.as_view(), name='social_account_list'),
    re_path(r'^socialaccounts/(?P<pk>\d+)/disconnect/$', SocialAccountDisconnectView.as_view(), name='social_account_disconnect'),
    path('ajax/', include('ajax.urls')), #ajax_views.main),
    path('api/', include('api.urls')), #ajax_views.main),
    path('blog/', include('blog.urls', namespace='blog')),
    path('admin/', admin.site.urls),
 #   path('captcha/', include('captcha.urls')),
    #path('api/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    #path('auth/', include('djoser.urls')),
    path('wiki/', include('wiki.urls')),
    path('notifications/', include('django_nyt.urls')),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

try:
    run_once()
except Exception as e:
    print('Startup problem: {}'.format(str(e)))
