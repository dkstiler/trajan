
from chat.models import ClientChannel


def run_once():
    # clear out all ClientChannel objects on startup
    ClientChannel.objects.all().delete()

